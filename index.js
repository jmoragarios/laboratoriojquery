$(document).ready(function() {
    
    $('#btnSubmit').click(function(){
        var tbody = $('#tbody');
        var row = $(tbody).find('.row')[0];
        var nRows = parseInt($('#txtRowsNumber').val());
        for(let i = 0; i < nRows; i++){
            let newRow = $(row).clone();
            $(newRow).removeAttr("hidden");
            $(tbody).append(newRow);
        }
    });

    $('#btnDelete').click(function(){
        var tbody = $('#tbody');
        var rows = $(tbody).find('.row');
        $(rows).each(function(){
            if(!($(this).attr("hidden"))){
                $(this).remove();
            }
        });
    });
});